﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public partial class Form4_2 : Form
    {
        public Form4_2()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double x, y, f;
            int i;

            x = double.Parse(txtX.Text);
            y = double.Parse(txtY.Text);

            f = Math.Pow(5.2, 3) * (Math.Log10(x+y)/(x-1/0.45*Math.Sin(x-8*y)));
            i = (int)f;

            txtF.Text = f.ToString();
            txtI.Text = i.ToString();

            txtFloorF.Text = Math.Floor(f).ToString();
            txtCeilF.Text = Math.Ceiling(f).ToString();
            txtFloorAbsF.Text = Math.Floor(Math.Abs(f)).ToString();
            txtCeilAbsF.Text = Math.Ceiling(Math.Abs(f)).ToString();

        }
    }
}
