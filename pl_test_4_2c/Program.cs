﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace pl_test_4_2
{
    class Program
    {
        static void Main(string[] args)
        {
            double x, y, f;
            int i;
            Console.WriteLine("Введите x: ");
            x = double.Parse(Console.ReadLine());
            
            Console.WriteLine("Введите y: ");
            y = double.Parse(Console.ReadLine());

            f = Math.Pow(5.2, 3d) * (Math.Log10(x + y)/(x - 1d/(0.45 * Math.Sin(x - 8d*y)))) + 0.5;
            i = (int)f;
            Console.WriteLine("f = Math.Pow(5.2, 3) * (Math.Log10(x + y)/(x-1 / 0.45 * Math.Sin(x - 8*y)))");
            Console.WriteLine("f = " + f.ToString());
            Console.WriteLine("i = " + i);
            Console.WriteLine("Floor(f) = " + Math.Floor(f).ToString());
            Console.WriteLine("Ceiling(f) = " + Math.Ceiling(f).ToString());
            Console.WriteLine("Floor(Abs(f)) = " + Math.Floor(Math.Abs(f)).ToString());
            Console.WriteLine("Ceiling(Abs(f)) = " + Math.Ceiling(Math.Abs(f)).ToString());
            Console.WriteLine("");
            Console.WriteLine("Нажмите любую клавишу...");
            Console.ReadKey();
        }
    }
}
