﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace pl_test_4_8_9
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private void fillArray(int[,] data)
        {
            Random rnd = new Random();
            int arrHeight = data.GetLength(0),
                arrWidth = data.GetLength(1);
            for (int i = 0; i < arrHeight; i++)
            {
                for (int j = 0; j < arrWidth; j++)
                {
                    data[i, j] = rnd.Next(-20, 20);
                }
            }
        }
        private void showMatrix(int[,] data)
        {
            int arrHeight = data.GetLength(0),
                arrWidth = data.GetLength(1);
            int[] row = new int[4];
            dgvSource.ColumnCount = arrWidth;
            for (int i = 0; i < arrHeight; i++)
            {
                dgvSource.Rows.Add();
                for (int j = 0; j < arrWidth; j++)
                {
                    dgvSource.Rows[i].Cells[j].Value = data[i, j];
                }
            }
        }
        private void showArray(int[] data)
        {
            foreach (int item in data)
            {
                lbxResult.Items.Add(item);
            }
        }
        private int[] filterArray(int[,] data)
        {
            int arrHeight = data.GetLength(0),
                arrWidth = data.GetLength(1);

            int[] result = new int[4];
            for (int i = 0; i < arrHeight; i++)
            {
                result[i] = 1;
                for (int j = 0; j < arrWidth; j++)
                {
                    if (data[i, j] > 0)
                    {
                        result[i] = data[i, j];
                        break;
                    }
                }
            }
            return result;
        }
        private void calc()
        {
            int[,] data = new int[4, 4];
            fillArray(data);
            showMatrix(data);
            int[] result = filterArray(data);
            showArray(result);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            calc();
        }
    }
}
