﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace pl_test_4_7
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void fillArray(int[] data)
        {
            Random rnd = new Random();
            for (int i = 0; i < data.Length; i++)
            {
                data[i] = rnd.Next(-15, 15);
            }

        }
        private void arrayOutput(int[] data, ListBox lbx)
        {
            lbx.Items.Clear();
            foreach (int item in data)
            {
                lbx.Items.Add(item);
            }
        }
        private int sumArray(int[] data)
        {
            int result = 0;
            foreach (int item in data)
            {
                result += item;
            }
            return result;
        }

        private void calc()
        {
            int[] data = new int[20];
            int j, i,
                firstPosIdx = -1,
                lastPosIdx = -1;

            fillArray(data);
            arrayOutput(data, lbxOrigArray);

            for (i = 0; i < data.Length; i++)
            {
                if (data[i] > 0)
                {
                    if (firstPosIdx < 0) firstPosIdx = i;
                    lastPosIdx = i;
                }
            }
            if (firstPosIdx == -1 || lastPosIdx == -1)
            {
                MessageBox.Show("В массиве нет положительных элементов");
                return;
            }

            int[] result = new int[lastPosIdx - firstPosIdx + 1];

            for (i = 0, j = firstPosIdx; i < result.Length; i++, j++)
            {
                result[i] = data[j];
            }
            arrayOutput(result, lbxResArray);
            tbxResult.Text = sumArray(result).ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            calc();
        }
    }
}
