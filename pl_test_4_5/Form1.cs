﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace pl_test_4_5
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private double calcEquation(double x)
        {
            return Math.Pow(x, 3d) - 6d * Math.Pow(x, 2d) + 9d * x + 4d;
        }
        private void calc()
        {
            double start = Double.Parse(tbxIntervalStart.Text),
                end = Double.Parse(tbxIntervalEnd.Text),
                step = Double.Parse(tbxStep.Text),
                item = start,
                y,
                max = calcEquation(start);
            int iterations = (int)((end - start) / step);
            for (int i = 0; i <= iterations; i++, item += step)
            {
                y = calcEquation(item);
                if (y > max) max = y;
                lbX.Items.Add(Math.Round(item, 3).ToString());
                lbY.Items.Add(Math.Round(y, 3).ToString());
            }
            tbxMax.Text = Math.Round(max, 3).ToString();
        }

        private void btnCalc_Click(object sender, EventArgs e)
        {
            lbX.Items.Clear();
            lbY.Items.Clear();
            calc();
        }
    }
}
