﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace pl_test_4_1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double a, b, c, p, Pp, s;

            if (txtA.Text == "" || txtB.Text == "" || txtC.Text == "")
            {
                MessageBox.Show("Заполните, пожалуйста, все поля.");
                return;
            }

            a = double.Parse(txtA.Text);
            b = double.Parse(txtB.Text);
            c = double.Parse(txtC.Text);
            if ((a > 0 && b > 0 && c > 0) && ((a + b - c) * (a + c - b) * (b + c - a) > 0))
            {
                p = a + b + c;
                Pp = p / 2;
                s = Math.Sqrt(Pp * (Pp - a) * (Pp - b) * (Pp - c));
                txtP.Text = p.ToString();
                txtS.Text = s.ToString();
            }
            else MessageBox.Show("Данные введены неверно!");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
