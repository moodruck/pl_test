﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace pl_test_4_3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double R, S;
            int n;
            n = int.Parse(txtSidesCount.Text);
            R = float.Parse(txtR.Text);
            S = Polygon.Area(n, R);

            txtPolySide.Text = Polygon.Side.ToString();
            txtPolyRadius.Text = Polygon.Radius.ToString();
            txtPolyArea.Text = S.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            txtPolyRadius.Clear();
            txtPolySide.Clear();
            txtPolyArea.Clear();
            txtR.Clear();
            txtSidesCount.Clear();
        }
    }
}
