﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace pl_test_4_3
{
    static class Polygon
    {
        static double a, r;

        public static double Side
        {
            get { return a; }
        }
        public static double Radius
        {
            get { return r; }
        }
        public static double Area(int n, double R)
        {
            a = 2 * R * Math.Sin(Math.PI / n);
            r = R * Math.Cos(Math.PI / n);
            return a * n * r / 2;
        }
    }
}
