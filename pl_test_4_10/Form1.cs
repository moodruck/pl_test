﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace pl_test_4_10
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private int getSymolsCount(string text, string symbols)
        {
            int counter = 0;
            foreach (char item in text)
            {
                foreach (char letter in symbols)
                {
                    if (letter == item)
                    {
                        counter++;
                        break;
                    }
                }
            }
            return counter;
        }
        private double getSymbolsPercentage(string text, string symbols)
        {
            int symbolsCount = getSymolsCount(text, symbols);
            double percentage = (double)symbolsCount / text.Length;
            return percentage;
        }
        private void showText(string[] text)
        {
            tbxSourceText.Clear();
            for (int i = 0; i < text.Length; i++)
            {
                tbxSourceText.Text += i.ToString() + ". " + text[i];
                tbxSourceText.Text += "\r\n\r\n";
            }
        }
        private int arrayMax(double[] data)
        {
            int maxIdx = 0;
            for (int i = 0; i < data.Length; i++)
            {
                if (data[i] > data[maxIdx]) maxIdx = i;
            }
            return maxIdx;
        }
        private void showArray(double[] data)
        {
            foreach (double item in data)
            {
                lbxResults.Items.Add(Math.Round(item,2).ToString());
            }
        }

        private double[] calcPercentage(string[] lines, string symbols)
        {
            double[] percentage = new double[lines.Length];
            for (int i = 0; i < lines.Length; i++)
            {
                percentage[i] = getSymbolsPercentage(lines[i], symbols);
            }

            return percentage;
        }
        private void readFile(string fileName)
        {
            string[] lines = System.IO.File.ReadAllLines(fileName);
            double[] percentage = calcPercentage(lines, tbxSymbols.Text);
            int maxIdx = arrayMax(percentage);
            showText(lines);
            showArray(percentage);
            tbxMax.Text = lines[maxIdx];
        }
        private void saveResults(string fileName)
        {
            System.IO.File.WriteAllText(fileName, tbxMax.Text, System.Text.Encoding.UTF8);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            readFile(openFileDialog1.FileName);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            openFileDialog1.InitialDirectory = Application.StartupPath;
        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            saveResults(saveFileDialog1.FileName);
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            saveFileDialog1.ShowDialog();
        }
    }
}
