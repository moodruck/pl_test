﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace pl_test_4_6
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void calc()
        {
            int n = 1;
            double i = 1d,
                res = 0d,
                eps = Double.Parse(tbxAcc.Text);

            while (Math.Abs(i) > eps)
            {
                res += i;

                n++;
                i = (double)Math.Pow(-1d, (double)n + 1d) * (1d / n);
            }
            tbxResult.Text = res.ToString();
            tbxTest.Text = Math.Log(2).ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            calc();
        }

    }
}
