﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace pl_test_4_4
{
    public partial class Form1 : Form
    {
        private double min(double x, double z)
        {
            if (x < z) return x;
            else return z;
        }
        private double max(double x, double y, double z)
        {
            double max = x;
            if (y > max) max = y;
            if (z > max) max = z;
            return max;
        }
        private double calc(double x, double z)
        {
            if (x < 0 && z < 0)
            {
                return min(x, z);
            }
            else if (x > 0 && z > 0)
            {
                double sum = x + z,
                       mult = x * z;

                return max(x, Math.Pow(sum, 1d / 3d), Math.Cos(mult));
            }
            else
            {
                return x + z;
            }

        }
        private void runCalc()
        {
            if (tbxX.Text == "" || tbxZ.Text == ""){
                MessageBox.Show("Не все поля заполнены!");
                return;
            }
            double x = double.Parse(tbxX.Text),
                   z = double.Parse(tbxZ.Text);
            tbxResult.Text = calc(x, z).ToString();
        }
        public Form1()
        {
            InitializeComponent();
        }

        private void btnCalc_Click(object sender, EventArgs e)
        {
            runCalc();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            tbxX.Clear();
            tbxZ.Clear();
            tbxResult.Clear();
        }
    }
}
