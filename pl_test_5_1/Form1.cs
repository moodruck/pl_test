﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

namespace pl_test_5_1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ofdGetData.InitialDirectory = Application.StartupPath;
            ofdGetData.ShowDialog();
        }
        private List<firm> loadFirmData(string fileName)
        {
            int i, j;
            string[] lines = System.IO.File.ReadAllLines(fileName);
            textBox1.Text = String.Join("\r\n",lines);
            
            string[] tmpLine = new string[2],
                tmpPrices = new string[5];
            firm tmpFirm;
            List<firm> orgs = new List<firm>();
            for (i = 0; i < lines.Length; i++)
            {
                if (lines[i].Substring(0, 2) != "##")
                {
                    tmpFirm = new firm();

                    tmpLine = lines[i].Split(':');
                    tmpFirm.name = tmpLine[0];
                    tmpPrices = tmpLine[1].Split(',');
                    tmpFirm.fee = Convert.ToDouble(tmpPrices[0], NumberFormatInfo.InvariantInfo);
                    for (j = 1; j < tmpPrices.Length; j++)
                    {
                        tmpFirm.prices.Add(Convert.ToDouble(tmpPrices[j], NumberFormatInfo.InvariantInfo));
                    }
                    orgs.Add(tmpFirm);
                }
            }
            return orgs;
        }

        private void showOrgs(List<firm> orgs)
        {
            int j;
            object[] orgRow = new object[6];
            //List<string> orgRow = new List<string>();
            foreach (firm org in orgs)
            {
                orgRow[0] = org.name;
                orgRow[1] = org.fee;
                for (j = 2; j < orgRow.Length; j++)
                {
                    // @TODO: Почитать про форматирование строк
                    orgRow[j] = org.prices[j - 2];
                }
                dataGridView1.Rows.Add(orgRow);
                //orgRow.Clear();
            }
        }
        private double[] getAverageProfit(List<firm> orgs)
        {
            int i;
            double[] profit = new double[orgs.Count];
            for (i = 0; i < profit.Length; i++)
            {
                profit[i] = 0;
                foreach (double price in orgs[i].prices)
                {
                    profit[i] += price;
                }
                profit[i] = profit[i] - profit[i] * orgs[i].fee;
            }
            return profit;
        }
        private void sortOrgs(List<firm> orgs)
        {
            int i, j;
            firm tmpFirm;
            double tmpProfit;
            double[] profit = getAverageProfit(orgs);

            for (i = 0; i < profit.Length; i++)
            {
                for (j = 0; j < profit.Length - 1; j++)
                {
                    if (profit[j] < profit[j + 1])
                    {
                        tmpProfit = profit[j];
                        profit[j] = profit[j + 1];
                        profit[j + 1] = tmpProfit;

                        tmpFirm = orgs[j];
                        orgs[j] = orgs[j + 1];
                        orgs[j + 1] = tmpFirm;
                    }
                }
            }
        }
        private void run(string fn)
        {
            dataGridView1.Rows.Clear();
            List<firm> orgs = loadFirmData(fn);
            sortOrgs(orgs);
            showOrgs(orgs);
        }
        private void saveData(string fn)
        {
            int i;
            List<string> tmpLine = new List<string>();
            string resultText = "##Название фирмы: Размер отчислений, Яйца, Зерно, Мясо, Молоко\r\n";
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                
                if (row.Cells[0].Value == null) continue;

                resultText += row.Cells[0].Value + ":";
                for (i = 1; i < row.Cells.Count; i++)
                {
                    tmpLine.Add(Convert.ToString(row.Cells[i].Value, NumberFormatInfo.InvariantInfo));
                }
                resultText += String.Join(",", tmpLine.ToArray()) + "\r\n";
                tmpLine.Clear();
            }
            System.IO.File.WriteAllText(fn, resultText);
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            run(ofdGetData.FileName);
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            sfdStoreData.ShowDialog();
        }

        private void sfdStoreData_FileOk(object sender, CancelEventArgs e)
        {
            saveData(sfdStoreData.FileName);
        }
    }
}
